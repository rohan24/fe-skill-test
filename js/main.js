$(function() {
    $('#ChangeToggle').click(function() {
      $('#navbar-hamburger').toggleClass('hidden');
      $('#navbar-close').toggleClass('hidden');  
    });


    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:4,
              nav:false,
              loop:false
          }
      }
  })
  
  $(".menu-icon").click(function(e) {
    e.stopPropagation();
    $("body").toggleClass('responsive-mode');
  });

    // Animation
    AOS.init({
        once: true
    })
    
    
  });